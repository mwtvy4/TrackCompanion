## takes csv data and puts it in double ended queue for graphing in real time

import csv
from bokeh.client import push_session
from bokeh.plotting import figure, show
from bokeh.io import curdoc
from bokeh.models import ColumnDataSource


##session = push_session(curdoc())

p = figure(plot_width=400,plot_height=400)
#y = collections.deque([0]*101)


source = ColumnDataSource(data=dict(x=[],y=[]))
p.line(x='x',y='y', source=source)

i = 1 #iterator for counting

def update():
    global i
    i=i+1

    f = open ('data.out','r',newline='')
    datain = f.readline()
    f.close()

    csvreader = csv.DictReader(datain.splitlines(), fieldnames=('first','second','third'),delimiter=',')
    for row in csvreader:
        global dataDict
        dataDict = row

    new_data = dict(x=[i],y=[dataDict['first']])
    #print(new_data)
    source.stream(new_data,100)


curdoc().add_root(p)
curdoc().title = "Plotting..."
curdoc().add_periodic_callback(update, 100)
